import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from '@kominal/core-user-service-angular-client';
import { CoreComponent } from './modules/core/core.component';
import { ObserverComponent } from './modules/observer/observer.component';

const routes: Routes = [
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: () => import('@kominal/core-user-service-angular-client').then((m) => m.AuthenticationModule),
	},
	{
		path: 'tenants/:tenantId',
		component: ObserverComponent,
		loadChildren: () => import('./modules/observer/observer.module').then((m) => m.ObserverModule),
	},
	{
		path: '',
		component: CoreComponent,
		loadChildren: () => import('./modules/core/core.module').then((m) => m.CoreModule),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule],
})
export class AppRoutingModule {}
