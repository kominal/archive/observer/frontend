import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Event } from 'src/app/core/models/event';
import { Log } from 'src/app/core/models/log';
import { MasterData } from '../../models/master-data';

@Injectable({
	providedIn: 'root',
})
export class ObserverHttpService {
	constructor(private httpClient: HttpClient) {}

	async getEvents(
		pagination: PaginationRequest,
		options: {
			projectName: string;
			environmentName: string;
			serviceName: string;
			start?: string;
			end?: string;
		}
	): Promise<PaginationResponse<Event>> {
		return this.httpClient
			.get<PaginationResponse<Event>>(`/controller/events/${options.projectName}/${options.environmentName}/${options.serviceName}`, {
				params: {
					start: options.start,
					end: options.end,
					...toPaginationParams(pagination),
				},
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	async aggregateEvents(options: {
		projectName: string;
		environmentName: string;
		serviceName: string;
		start?: Date;
		end?: Date;
		type?: string;
		aggregation: string;
		field?: string;
	}): Promise<any> {
		return this.httpClient
			.get<{ name: string; series: { name: string; value: number }[] }[]>(
				`/controller/events/aggregate/${options.projectName}/${options.environmentName}/${options.serviceName}`,
				{
					params: this.toParams({
						start: options.start,
						end: options.end,
						type: options.type,
						aggregation: options.aggregation,
						field: options.field,
					}),
					headers: AUTHENTICATION_REQUIRED,
				}
			)
			.toPromise();
	}

	async getLogs(
		pagination: PaginationRequest,
		options: {
			projectName: string;
			environmentName: string;
			serviceName: string;
			maxId?: string;
			start?: string;
			end?: string;
		}
	): Promise<PaginationResponse<Log>> {
		return this.httpClient
			.get<PaginationResponse<Log>>(`/controller/logs/${options.projectName}/${options.environmentName}/${options.serviceName}`, {
				params: {
					...this.toParams(options),
					...toPaginationParams(pagination),
				},
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	async getMasterData(tenantId: string): Promise<MasterData> {
		return this.httpClient
			.get<MasterData>(`/controller/tenants/${tenantId}/masterData`, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}
	toParams(params: { [param: string]: any }): { [param: string]: string } | undefined {
		const cleanedParams: { [param: string]: string } = {};
		for (const key of Object.keys(params)) {
			if (params[key]) {
				cleanedParams[key] = params[key];
			}
		}
		return Object.keys(cleanedParams).length > 0 ? cleanedParams : undefined;
	}
}
