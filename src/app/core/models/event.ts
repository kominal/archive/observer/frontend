export interface Event {
	_id?: string;
	projectName: string;
	environmentName: string;
	serviceName: string;
	taskId: string | undefined;
	time: Date;
	type: string;
	content: any;
	message: never;
}
