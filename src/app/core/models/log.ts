export interface Log {
  _id?: string;
  projectName: string;
  environmentName: string;
  serviceName: string;
  taskId: string;
  time: Date;
  level: string;
  message: any;
}
