export interface MasterData {
	[project: string]: {
		[environment: string]: string[];
	};
}
