import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-aggregation-insights',
	templateUrl: './aggregation-insights.component.html',
	styleUrls: ['./aggregation-insights.component.scss'],
})
export class AggregationInsightsComponent {
	rangeOptions = new BehaviorSubject(['5m', '30m', '1h', '8h', '1d', '7d', '14d', 'custom']);
	aggregationOptions = new BehaviorSubject(['value', 'count', 'min', 'max', 'sum']);
	resolutionOptions = new BehaviorSubject(['1m', '5m', '1h', '8h', '1d']);

	results = [];

	form = new FormGroup({
		range: new FormControl('5m', Validators.required),
		aggregation: new FormControl('value', Validators.required),
		resolution: new FormControl('none'),
		start: new FormControl(),
		end: new FormControl(),
		type: new FormControl(),
		field: new FormControl(),
	});

	path: { projectName: string; environmentName: string; serviceName: string } | undefined;

	constructor(public observerService: ObserverService) {
		observerService.currentMasterDataSubject.subscribe(({ projectName, environmentName, serviceName }) => {
			if (projectName && environmentName && serviceName) {
				this.path = { projectName, environmentName, serviceName };
				this.loadResult();
			}
		});
		this.form.valueChanges.pipe(debounceTime(1000)).subscribe(() => {
			this.loadResult();
		});
	}

	async loadResult() {
		if (!this.path) {
			return;
		}

		if (this.form.invalid) {
			return;
		}

		const formValue = this.form.value;

		let start: Date | undefined = undefined;
		let end: Date | undefined = undefined;

		if (!formValue.range) {
			return;
		}

		if (formValue.range === 'absolute') {
			start = formValue.start;
			end = formValue.end;
		} else {
			start = new Date();
			end = new Date();

			if (formValue.range === '5m') {
				start.setMinutes(-1);
			} else if (formValue.range === '30m') {
				start.setMinutes(-30);
			} else if (formValue.range === '1h') {
				start.setHours(-1);
			} else if (formValue.range === '8h') {
				start.setHours(-8);
			} else if (formValue.range === '1d') {
				start.setDate(-1);
			} else if (formValue.range === '7d') {
				start.setDate(-6);
			} else if (formValue.range === '14d') {
				start.setDate(-14);
			}
		}

		if (!start || !end) {
			return;
		}

		this.results = await this.observerService.aggregateEvents({
			...this.path,
			start,
			end: new Date(),
			type: 'SERVICE_UP',
			aggregation: formValue.aggregation,
			field: 'content.duration',
		});
	}
}
