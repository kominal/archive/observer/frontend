import { Component } from '@angular/core';
import { TenantService } from '@kominal/core-user-service-angular-client';
import { Tenant } from 'src/app/core/models/tenant';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-breadcrumbs',
	templateUrl: './breadcrumbs.component.html',
	styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent {
	constructor(public observerService: ObserverService, public tenantService: TenantService<Tenant>) {}
}
