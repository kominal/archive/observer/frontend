import { Component, Input } from '@angular/core';
import { Event } from 'src/app/core/models/event';

@Component({
	selector: 'app-event',
	templateUrl: './event.component.html',
	styleUrls: ['./event.component.scss'],
})
export class EventComponent {
	JSON = JSON;

	@Input() event!: Event;
}
