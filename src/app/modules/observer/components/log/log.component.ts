import { Component, Input } from '@angular/core';
import { Log } from 'src/app/core/models/log';

@Component({
	selector: 'app-log',
	templateUrl: './log.component.html',
	styleUrls: ['./log.component.scss'],
})
export class LogComponent {
	@Input() log!: Log;
}
