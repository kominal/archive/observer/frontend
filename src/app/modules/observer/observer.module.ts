import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormModule } from '@kominal/lib-angular-form';
import { TableModule } from '@kominal/lib-angular-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from 'src/app/shared/shared.module';
import { AggregationInsightsComponent } from './components/aggregation-insights/aggregation-insights.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { EventComponent } from './components/event/event.component';
import { FilterComponent } from './components/filter/filter.component';
import { HistoryInsightsComponent } from './components/history-insights/history-insights.component';
import { LogComponent } from './components/log/log.component';
import { ObserverRoutingModule } from './observer-routing.module';
import { ObserverComponent } from './observer.component';
import { EnvironmentsComponent } from './pages/environments/environments.component';
import { InsightsComponent } from './pages/insights/insights.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ServicesComponent } from './pages/services/services.component';

@NgModule({
	declarations: [
		ObserverComponent,
		FilterComponent,
		ProjectsComponent,
		EnvironmentsComponent,
		ServicesComponent,
		InsightsComponent,
		BreadcrumbsComponent,
		LogComponent,
		EventComponent,
		AggregationInsightsComponent,
		HistoryInsightsComponent,
	],
	imports: [CommonModule, ObserverRoutingModule, TableModule, SharedModule, FormModule, NgxChartsModule],
})
export class ObserverModule {}
