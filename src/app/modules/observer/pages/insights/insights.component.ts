import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-insights',
	templateUrl: './insights.component.html',
	styleUrls: ['./insights.component.scss'],
})
export class InsightsComponent {
	constructor(public observerService: ObserverService, activatedRoute: ActivatedRoute) {
		activatedRoute.params.subscribe((params) => {
			observerService.currentMasterDataSubject.next(params);
		});
	}
}
