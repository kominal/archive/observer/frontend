import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './material.module';
import { HumanReadablePipe } from './pipes/human-readable/human-readable.pipe';

@NgModule({
	imports: [TranslateModule, MaterialModule, FormsModule, ReactiveFormsModule],
	exports: [TranslateModule, MaterialModule, FormsModule, ReactiveFormsModule, HumanReadablePipe],
	declarations: [HumanReadablePipe],
})
export class SharedModule {}
